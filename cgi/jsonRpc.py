#! /home/klevstul/virtualenvs/py2.7/bin/python
# -*- coding: iso-8859-1 -*-

# win C:\prgFiles\Python27\pythonw.exe
# py24 /usr/bin/python



import os

# set environment variables
#os.environ['ORACLE_HOME']		= '/home/klevstul/.OIC/instantclient10_1'
#os.environ['NLS_LANG']			= 'NORWEGIAN_NORWAY.WE8MSWIN1252'

# import other modules
import cx_Oracle
import json
import cgi
import cgitb

# write error messages in a web supported format
cgitb.enable()


def outputJson(connectionString, procedure, module, action, username, password, ipAddress, sessionKey, id):
	"""Call PL/SQL procedure that returns a ref cursor and output database content as JSON"""
	connection	= cx_Oracle.connect(connectionString)
	cursor		= connection.cursor()
	retCursor	= connection.cursor()
	columnName	= []

	if module == 'None':
		module = 'help'

	if action == 'None' and module == 'help':
		action = 'overview'
	elif action == 'None':
		action = ''

	if username != 'None':
		action += '-u-' + username
	else:
		action += '-u-'

	if password != 'None':
		action += '-p-' + password
	else:
		action += '-p-'

	if ipAddress == 'false':
		action += '-a-'
	else:
		action += '-a-' + os.environ["REMOTE_ADDR"]

	if sessionKey != 'None':
		action += '-k-' + sessionKey
	else:
		action += '-k-'

	if id != 'None':
		action += '-i-' + id
	else:
		action += '-i-0'

	retCursor = cursor.callfunc(procedure, cx_Oracle.CURSOR, [module, action])

	print 'Content-Type: text/plain; charset="ISO-8859-1"'
	print ''

	print '{'
	print '"result_set" : '															# everything is returned with root "result_set", alternative code would be [print '"'+module+'" : ']
	print '['

	for x in retCursor.description:
		columnName.append(x[0])

	for rowData in retCursor:
		rowObject = {}

		if retCursor.rowcount > 1:
			print ','

		for idx, x in enumerate(rowData):
			rowObject[columnName[idx]] = x

		print json.dumps(
			  rowObject
			, ensure_ascii = False
			, encoding = 'utf-8'
			, indent = 4
		)

	print ']'
	print ', "hits" : ' + str(retCursor.rowcount)
	print ', "success" : true'
	print '}'

	cursor.close()
	retCursor.close()
	connection.close()


def getCgiParameters():
	form = cgi.FieldStorage()
	parameters =(
		{
			  'module'		: form.getvalue('p_module')
			, 'action'		: form.getvalue('p_action')
			, 'username'	: form.getvalue('p_username')
			, 'password'	: form.getvalue('p_password')
			, 'ipaddress'	: form.getvalue('p_ip_address')
			, 'sessionkey'	: form.getvalue('p_session_key')
			, 'id'			: form.getvalue('p_id')
		}
	)
	return parameters


def main():
	params = getCgiParameters()
	outputJson('sleeptracker_rpc/YWg37EHo@//privyinstance.cdhzvkd198ux.us-east-1.rds.amazonaws.com:1521/PRIVYDB', 'sleeptracker.rpc.call', str(params['module']), str(params['action']), str(params['username']), str(params['password']), str(params['ipaddress']), str(params['sessionkey']), str(params['id']))


if "__main__" == __name__:
	main()
