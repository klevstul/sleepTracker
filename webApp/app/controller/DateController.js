/*
	what:		Dates to control displayed data
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.DateController', {
      extend:	'Ext.Container'
	, alias:	'widget.datecontroller'
	, layout: {
		type:	'column'
	  }
	, margin:	'10 10 10 10'
	, items: [{
		  xtype:		'datefield'
		, fieldLabel:	'Start date'
		, format:		'd.m.Y'
		, itemId:		'datefield-startdate'
		, labelWidth:	65
		, width:		180
	  },{
		  xtype:		'datefield'
		, margin:		'0 0 0 10'
		, fieldLabel:	'End date'
		, itemId:		'datefield-enddate'
		, format:		'd.m.Y'
		, labelWidth:	65
		, width:		180
      },{
		  margin:	'0 0 0 10'
		, html:		'[past 10d] [past 30d] [past 60d] [past 180d] [past 365d] [all]'
	  },{
		  xtype:	'button'
		, margin:	'0 0 0 10'
		, text:		'Submit'
		, listeners: {
			click: function() {
				var fromDate	= this.ownerCt.getComponent('datefield-startdate').getValue();
				var toDate		= this.ownerCt.getComponent('datefield-enddate').getValue();
				var stores		= this.ownerCt.getStores();

				Ext.each(stores, function(store){
					// add filter
					if (fromDate !== undefined && toDate !== undefined && fromDate !== null && toDate !== null){
						store.filterBy(
							function( rec, id ){
								return rec.data.timestamp_sleep >= fromDate && rec.data.timestamp_sleep < toDate;
							}
						);
					// remove filter
					} else {
						store.filterBy(function( rec, id ){return true;});
					}
				});
			}
		  }
	  },{
		  xtype:	'button'
		, margin:	'0 0 0 10'
		, text:		'Reload'
		, listeners: {
			click: function() {
				// note: we only have to reload sleeprecorddaygrid's store, as sleeprecordgrid's store is reloaded as part of the callback (see login code in LoginWindow.js)
				Ext.ComponentQuery.query('sleeprecorddaygrid')[0].getStore().reload();
			}
		  }
	  }
	  ]
	, getStores: function(){
		var stores = [];
		stores.push(Ext.ComponentQuery.query('sleeprecordgrid')[0].getStore());
		stores.push(Ext.ComponentQuery.query('sleeprecorddaygrid')[0].getStore());
		return stores;
	}

});