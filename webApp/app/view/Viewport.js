/*

	what:		Viewport, where it all starts...
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)

*/
//"use strict";

Ext.define('SleepTracker.view.Viewport', {
	  extend: 'Ext.container.Viewport'
/*	requires: [
		  'CommissionManager.view.MainLayout'
		, 'CommissionManager.view.CommissionList'
		, 'CommissionManager.view.TrueOrFalseCombo'
		, 'CommissionManager.view.CommissionTypeCombo'
	],
*/
	, layout: 'fit'
	, initComponent: function() {
		this.items = {
			  xtype: 'panel'
			, layout: 'fit'
			, items: [
				{
                    xtype: 'mainlayout'
                }
				,
				{
					xtype: 'loginwindow'
                }
			  ]
		};
		console.log('Viewport()'); 
		this.callParent();
	}
});
