/*
	what:		Showing the stats for our dataThe presentation view, for a (visual) presentation of the data
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.StatisticsView', {
	  extend:		'Ext.panel.Panel'
	, alias:		'widget.statisticsview'
	, itemId:		'statisticsview'
	, plain:		true
	, items: {
		html: 'Number of records: <span id="statisticsview_records"></span> <br/>Number of days: xx <br/>Total sleep time: xx hours<br/>Average daily sleep: xx hours'
	  }
	, loadstats: function(){
		var store = Ext.ComponentQuery.query('sleeprecordgrid')[0].getStore();
		Ext.DomQuery.selectNode('#statisticsview_records').innerHTML = store.getCount();		
	  }
	, initComponent: function() {
		console.log('StatisticsView()');
		this.callParent();
	}
});
