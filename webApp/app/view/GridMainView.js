/*
	what:		A panel for presenting different grids
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.GridMainView', {
	  extend:		'Ext.TabPanel'
	, alias:		'widget.gridmainview'
	, activeTab:	0
	, plain:		true
	, defaults :{
		  autoScroll: true
		, bodyPadding: 10
	}
	, items: [{
		  title: 'Sleep records - day'
		, items: {xtype: 'sleeprecorddaygrid'}
		, listeners: {
			activate: function(tab) {
				var trendView = Ext.ComponentQuery.query('trendview')[0];
				var store = Ext.getStore('SleepRecordDays');
				trendView.setStore(store, 'sleeprecorddaygrid');
			}
		  }
	  }
	  ,
	  {
		  title: 'Sleep records - entries'
		, items: {xtype: 'sleeprecordgrid'}
		, listeners: {
			activate: function(tab) {
				var trendView = Ext.ComponentQuery.query('trendview')[0];
				var store = Ext.getStore('SleepRecords');
				trendView.setStore(store, 'sleeprecordgrid');
			}
		  }
	  }]
	, initComponent: function() {
		console.log('GridMainView()');
		this.callParent();
	}
});
