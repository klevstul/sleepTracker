/*

	what:		Main layout for the application
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)

	-- LAYOUT --

    |-------------------------------------------------|
    |                  Sleep Tracker                  |
    |-------------------------------------------------|
    | |---------------------------------------------| |
    | |             [date controller]               | |
    | |---------------------------------------------| |
    | |---------------| |---------------------------| |
    | | sleep records | |     presentation view     | |
    | |...............| |---------------------------| |
    | |...............| |  [presentation selector]  | |
    | |...............| | |-----------------------| | |
    | |...............| | |   data presentation   | | |
    | |...............| | |.......................| | |
    | |...............| | |.......................| | |
    | |...............| | |.......................| | |
    | |...............| | |.......................| | |
    |-------------------------------------------------|

*/

Ext.define('SleepTracker.view.MainLayout', {
      extend:		'Ext.panel.Panel'
	, alias:		'widget.mainlayout'
	, itemId:		'mainlayout'
	, title:		Global.parameters.applicationName
	, layout: 		'border'
	, bodyStyle:	'padding:5px;background:#eee;'
	, defaults: {
		  collapsible:	true
		, split:		true
		, bodyPadding:	5
	  }
	, items: [
		{
			  title:		'Date selector'
			, region:		'north'
			, height:		100
			, minHeight:	75
			, maxHeight:	250
			, items: {
				xtype:		'datecontroller'
			  }
		}
		,
		{
			  collapsible:	false
			, itemId:		'mainlayout_west'
			, region:		'west'
			, width:		850
			, items: {
				xtype: 		'gridmainview'
			  }
		}
		,
		{
			  collapsible:	false
			, region:		'center'
			, items: [
				{
					  region:	'north'
					, xtype:	'presentationview'
				}
			  ]
		}
		,
		{
			  title: 'v ' + Global.parameters.applicationVersion
			, region: 'south'
			, height: 60
			, collapsed: true
			, html: Global.parameters.applicationName + ' // by ' + Global.parameters.credits + ' // <span id="SleepTracker.MainLayout.south.loggedInMessage">...</span>'
		}
	  ]
	, initComponent: function() {
		console.log('MainLayout()');
		this.callParent();
	  }
});
