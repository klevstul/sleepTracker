/*
	what:		Showing the stats for our dataThe presentation view, for a (visual) presentation of the data
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/


/*
http://docs.sencha.com/ext-js/4-2/extjs-build/examples/charts/Line.html
http://docs.sencha.com/ext-js/4-2/extjs-build/examples/charts/Line.js
http://docs.sencha.com/ext-js/4-2/extjs-build/examples/example-data.js
*/

Ext.define('SleepTracker.view.TrendView', {
	  extend:	'Ext.chart.Chart'
	, alias:	'widget.trendview'
	, itemId:	'trendview'
	, width:	800
	, height:	400
	, style:	'background:#fff'
	, animate:	true
	, store:	'SleepRecords'
	, shadow:	true
	, theme:	'Blue'
	, gridName:	null
	, axes: [
		{
			  type: 'Numeric'
			, minimum: 0
			, position: 'left'
			, fields: ['MINUTES']
			, title: 'Minutes'
			, minorTickSteps: 1
			, grid: {
                odd: {
					  opacity:			1
					, fill:				'#ddd'
					, stroke:			'#bbb'
					, 'stroke-width':	0.1
                }
              }
        }
		,
		{
			  type:		'Category'
			, position:	'bottom'
			, fields:	['TIMESTAMP_SLEEP']
			, title:	'Day'
        }
	  ]
	, series: [
		{
			  type: 'line'
			, highlight: {
                  size: 7
				, radius: 7
              }
			, axis:		'left'
			, smooth:	true
			, fill:		true
			, xField:	'TIMESTAMP_SLEEP'
			, yField:	'MINUTES'
			, markerConfig: {
                  type:				'circle'
				, size:				4
				, radius:			4
				, 'stroke-width':	0
              }
			, tips: {
				  trackMouse: true
				, width: 300
				, height: 80
				, renderer: function(storeItem, item) {
					this.setTitle(storeItem.get('TIMESTAMP_SLEEP'));
					var hours = storeItem.get('MINUTES') / 60;
					hours = Math.round( hours * 10 ) / 10;
					this.update(hours + ' hours' + '<br/><br/>' + storeItem.get('NOTE'));
				  }
			  }
			, listeners: {
				itemmousedown : function(obj) {								
					var gridName = Ext.ComponentQuery.query('trendview')[0].gridName;								
					var grid = Ext.ComponentQuery.query( gridName )[0]; // ComponentQuery returns an array, hence we need to use [0] to get the first element
					var sm = grid.getSelectionModel();

					sm.select( obj.storeItem.index , true);
				}
			  }
		}
	  ]
	, setStore: function(store, gridname){
		this.gridName = gridname;
		this.bindStore(store);		
	  }
	, initComponent: function() {
		console.log('TrendView()');
		this.callParent();
	  }
});
