PROMPT *** rpc ***
set define off
set serveroutput on





-- ******************************************************
-- *													*
-- *				package head						*
-- *													*
-- ******************************************************
create or replace package rpc is
	TYPE csGetResultSet is REF CURSOR;

	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- WARNING: Never edit this code directly using TOAD or another Oracle tool.
	--          The updates should always be done editing the original script, and
	--          then be compiled into the database using TOAD or SQL*Plus
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- Package welcome:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- rpc
	--
	-- Package containing code for remote procedure calls (rpc).
	--
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- How to make comments in the code:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	-- ex 1:
	-- ----------------------------------------------------------------------
	-- Comment a bulk of code
	-- For example if you want to comment the next portion of the code, like
	-- an while loop, if statment or something similar.
	-- ----------------------------------------------------------------------

	-- ex 2:
	-- ---
	-- important comment for the next line(s) of code
	-- ---

	-- ex 3:
	-- comment the next line, or just the next lines

	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	--
	-- *** Revision | Newest version at the top
	--
	-- 130502 : Frode Klevstul : started
	--
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	procedure i;
	function call
	(
		p_module				varchar2							default null
	  , p_action				varchar2							default null
	) return csGetResultSet;

end;
/
show errors;





-- ******************************************************
-- *													*
-- *				package body						*
-- *													*
-- ******************************************************
create or replace package body rpc is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	g_package constant varchar2(16)	:= 'rpc';

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        i(nfo)
-- what:        info procedure
-- author:      Frode Klevstul
-- start date:  2013.05.02
---------------------------------------------------------
procedure i
is
	c_procedure					constant varchar2(32)					:= 'i';
begin
	dbms_output.put_line(to_char(sysdate,'YYDDMM HH24:MI:SS') ||': '||g_package||'.'||c_procedure);
end i;


---------------------------------------------------------
-- name:        call
-- what:        Remote Procedure Call / to be used externally
-- author:      Frode Klevstul
-- start date:  2013.05.02
---------------------------------------------------------
function call
(
    p_module				varchar2							default null
  , p_action				varchar2							default null
) return csGetResultSet
is
	c_procedure				constant varchar2(16)				:= 'call';

	c_action				constant varchar2(500)							:= substr( p_action, 0, (instr(p_action, '-u-')-1) );
	c_username				constant use_user.username%type					:= substr( p_action, instr(p_action,'-u-')+3,(instr(p_action, '-p-')-instr(p_action, '-u-')-3));
	c_password				constant use_user.password%type					:= substr( p_action, instr(p_action, '-p-')+3,(instr(p_action, '-a-')-instr(p_action, '-p-')-3));
	c_ip					constant use_ip_restriction.ip_address%type		:= substr( p_action, instr(p_action, '-a-')+3,(instr(p_action, '-k-')-instr(p_action, '-a-')-3));
	c_session_key			constant ses_session.session_key%type			:= substr( p_action, instr(p_action, '-k-')+3,(instr(p_action, '-i-')-instr(p_action, '-k-')-3));
	c_id					constant int									:= to_number(substr( p_action, instr(p_action, '-i-')+3, length(p_action) ));

	c_dateFormat			constant varchar2(30)							:= sys_lib.getVariableValue('rpc_date_format');
	c_dateFormatShort		constant varchar2(30)							:= sys_lib.getVariableValue('rpc_date_format_short');
	c_code_module			constant ses_session.last_code_module%type		:= g_package||'.'||c_procedure;
	c_parameter_list		constant ses_session.last_parameter_list%type	:= 'p_module='||p_module||'&p_action='||p_action;

	v_csGetData				csGetResultSet;
	v_sql					varchar2(2000);
	v_validSession			boolean								:= true;
	v_return_message		varchar2(100);

	v_user_pk				use_user.pk%type;
	v_session_key			ses_session.session_key%type;

begin

	-- check if session is valid
	if (p_module != 'login' and p_module != 'help') then
		v_validSession := ses_lib.validateSession(
							  p_session_key			=> c_session_key
							, p_ip_address			=> c_ip
							, p_last_code_module	=> c_code_module
							, p_last_parameter_list	=> c_parameter_list
							, p_return_message		=> v_return_message
						);
	end if;

	if (v_validSession = false) then
		v_sql := '
			select	2 as bool_error
				,	'''|| v_return_message ||''' as status_message
			from	dual
		';
	elsif (p_module = 'help' and c_ip is not null) then
		if (p_action like 'overview%') then
			v_sql := '
				select	help_text
				from	hel_help
				order by pk asc
			';
		elsif (p_action like 'desc_rpc%') then
			v_sql := '
				select	object_name as procedure
					,	argument_name
					,	data_type
					,	in_out
					,	default_value as "default value"
				from	user_arguments where package_name = ''RPC''
				order by object_name, argument_name
			';
		elsif (p_action like 'src_head_rpc%') then
			v_sql := '
				select	text
				from	user_source
				where	name = ''RPC''
				and		type=''PACKAGE''
				order by line asc
			';
		else
			v_sql := '
				select	1 as bool_error
					,	''unknown action: '||p_module||'-'||p_action||''' as status_message
				from	dual
			';
		end if;
	elsif (p_module = 'sleep_record') then
		if (p_action like 'list%') then
			v_sql := '
				select
						0 as bool_error
					,	pk
					,	to_char(timestamp_sleep, '''||c_dateFormat||''') as timestamp_sleep
					,	to_char(timestamp_wake, '''||c_dateFormat||''') as timestamp_wake
					,	minutes
					,	note
					,	timezone_adjustment
					,	bool_daylight_saving
					,	bool_standard_time
					,	to_char(timestamp_insert, '''||c_dateFormat||''') as timestamp_insert
					,	to_char(timestamp_update, '''||c_dateFormat||''') as timestamp_update
				from	rec_sleep_record
				where	pk in (
					select	rec_sleep_record_fk_pk
					from	rec_use_join
					where	use_user_fk_pk = (
						select	use_user_fk
						from	ses_session
						where	session_key = '''||c_session_key||'''
					)
				)
				order by timestamp_sleep asc
			';
		else
			v_sql := '
				select	1 as bool_error
					,	''unknown action: '||p_module||'-'||p_action||''' as status_message
				from	dual
			';
		end if;
	elsif (p_module = 'sleep_record_day') then
		if (p_action like 'list%') then
			v_sql := '
				with one as (
					select	sum(minutes) as minutes
						,	(trunc(timestamp_sleep) - 12/24) as timestamp_sleep
					from	rec_sleep_record
					group by (trunc(timestamp_sleep) - 12/24)
				)
				select	0 as bool_error
					,	max(rsl.pk) as pk
					,	to_char((one.timestamp_sleep), '''||c_dateFormatShort||''') as timestamp_sleep
					,	one.minutes
				from	rec_sleep_record rsl
					,	one
				where	(trunc(rsl.timestamp_sleep) - 12/24) = one.timestamp_sleep
				group by one.timestamp_sleep, one.minutes
				order by pk
			';
		else
			v_sql := '
				select	1 as bool_error
					,	''unknown action: '||p_module||'-'||p_action||''' as status_message
				from	dual
			';
		end if;
	elsif (p_module = 'iud_u_rec_sleep_record_note') then
		if (p_action is not null and c_id <> 0) then

			update	rec_sleep_record
			set		note = c_action
			where	pk = c_id;
			commit;

			v_sql := '
				select	0 as bool_error
					,	''update success'' as status_message
				from dual
			';
		elsif (c_id = 0) then
			v_sql := '
				select	1 as bool_error
					,	''missing id, c_id ('||c_id||')'' as status_message
				from dual
			';
		else
			v_sql := '
				select	1 as bool_error
					,	''unknown action: '||p_module||'-'||p_action||''' as status_message
				from	dual
			';
		end if;
	elsif (p_module = 'login') then
		if (p_action like 'verify%') then

			-- validate username and password
			v_user_pk := ses_lib.validateUser(c_username, c_password);

			if (v_user_pk is not null) then
				-- if the request is from a valid ip for given user
				if ( ses_lib.validateIp(v_user_pk, c_ip) ) then
					-- generate session key
					v_session_key := ses_lib.generateSessionKey;

					-- set session
					ses_lib.setSession(
						  p_user_pk				=> v_user_pk
						, p_ip_address			=> c_ip
						, p_session_key			=> v_session_key
						, p_last_code_module	=> c_code_module
						, p_last_parameter_list => c_parameter_list
					);

					v_sql := '
						select	0 as bool_error
							,	''Login success'' as status_message
							,	'''||v_session_key||''' as session_key
							,	'''||c_username||''' as username
						from	dual
					';
				else
					v_sql := '
						select	1 as bool_error
							,	''Invalid login location'' as status_message
						from	dual
					';
				end if;
			else
				v_sql := '
					select	1 as bool_error
						,	''Invalid user credentials'' as status_message
					from	dual
				';
			end if;
		else
			v_sql := '
				select	1 as bool_error
					,	''unknown action: '||p_module||'-'||p_action||''' as status_message
				from	dual
			';
		end if;
	else
		v_sql := '
			select	1 as bool_error
				,	''unknown module-action combination: '||p_module||'-'||p_action||''' as status_message
			from	dual
		';
	end if;

	open v_csGetData for
		v_sql;

	-- log user activity
	log_lib.userActivity
	(
		  p_session_key		=> c_session_key
		, p_username		=> c_username
		, p_code_module		=> c_code_module
		, p_parameter_list	=> c_parameter_list
		, p_details			=> v_sql ||'; '|| v_return_message
	);

	-- return a result set 
	return v_csGetData;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		null;
end call;





-- ******************************************** --
end;
/
show errors;