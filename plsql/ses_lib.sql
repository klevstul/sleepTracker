PROMPT *** ses_lib ***
set define off
set serveroutput on





-- ******************************************************
-- *													*
-- *				package head						*
-- *													*
-- ******************************************************
create or replace package ses_lib is
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- WARNING: Never edit this code directly using TOAD or another Oracle tool.
	--          The updates should always be done editing the original script, and
	--          then be compiled into the database using TOAD or SQL*Plus !!!
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- Package overview:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- name: ses_lib
	--
	-- Package containing session module related code.
	--
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- How to make comments in the code:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	-- ex 1:
	-- ----------------------------------------------------------------------
	-- Comment a bulk of code
	-- For example if you want to comment the next portion of the code, like
	-- an while loop, if statment or something similar.
	-- ----------------------------------------------------------------------

	-- ex 2:
	-- ---
	-- important comment for the next line(s) of code
	-- ---

	-- ex 3:
	-- comment the next line(s) of code

	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	--
	-- *** Revision | Newest version at the top
	--
	-- 130506 : Frode Klevstul : started
	--
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	procedure i;
	function validateUser
	(
		  p_username				in use_user.username%type
		, p_password				in use_user.password%type
	) return use_user.pk%type;
	function validateIp
	(
		  p_user_pk					in use_user.pk%type
		, p_ip_address				in use_ip_restriction.ip_address%type
	) return boolean;
	function validateSession
	(
		  p_session_key				in ses_session.session_key%type
		, p_ip_address				in use_ip_restriction.ip_address%type
		, p_last_code_module		in ses_session.last_code_module%type
		, p_last_parameter_list		in ses_session.last_parameter_list%type
		, p_return_message			out varchar2
	) return boolean;
	function generateSessionKey
		return ses_session.session_key%type;
	procedure setSession
	(
		  p_user_pk					in use_user.pk%type
		, p_ip_address				in ses_session.ip_address%type
		, p_session_key				in ses_session.session_key%type
		, p_last_code_module		in ses_session.last_code_module%type
		, p_last_parameter_list		in ses_session.last_parameter_list%type
	);
	function getSessionPk
	(
		  p_session_key				in ses_session.session_key%type
	
	) return ses_session.pk%type;
	function timeout
	(
		  p_session_pk				in ses_session.pk%type
	) return number;

end;
/
show errors;





-- ******************************************************
-- *													*
-- *				package body						*
-- *													*
-- ******************************************************
create or replace package body ses_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	g_package	constant varchar2(16)	:= 'ses_lib';
	g_debug		constant boolean		:= false;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        i(nfo)
-- what:        info procedure
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
procedure i
is
	c_procedure					constant varchar2(32)					:= 'i';
begin
	dbms_output.put_line(to_char(sysdate,'YYMMDD HH24:MI:SS') ||': '||g_package||'.'||c_procedure);
end i;


---------------------------------------------------------
-- name:        validateUser
-- what:        checks if username and password is valid,
--				returns pk if valid otherwise null
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
function validateUser
(
	  p_username				in use_user.username%type
	, p_password				in use_user.password%type
) return use_user.pk%type
is
	c_procedure					constant varchar2(32)					:= 'validateUser';

	v_count						int;
	v_pk						use_user.pk%type;

begin

	select	count(*)
	into	v_count
	from	use_user
	where	username = p_username
	and		password = p_password;
	
	if (v_count = 1) then
		select	pk
		into	v_pk
		from	use_user
		where	username = p_username
		and		password = p_password;
	elsif (v_count > 1) then
		raise_application_error(-20000, 'several users with username ['||p_username||'] and password ['||p_password||'], which should be impossible');
	end if;

	return v_pk;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		return null;
end validateUser;


---------------------------------------------------------
-- name:        validateIp
-- what:        checks if ip address is valid for user
-- author:      Frode Klevstul
-- start date:  2013.05.08
---------------------------------------------------------
function validateIp
(
	  p_user_pk					in use_user.pk%type
	, p_ip_address				in use_ip_restriction.ip_address%type
) return boolean
is
	c_procedure					constant varchar2(32)					:= 'validateIp';

	v_count						int;
	v_return					boolean;

begin
	select	count(*)
	into	v_count
	from	use_ip_restriction
	where	use_user_fk = p_user_pk
	and		ip_address = p_ip_address;

	if (v_count > 0) then
		v_return := true;
	else
		v_return := false;
	end if;

	return v_return;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		return null;
end validateIp;


---------------------------------------------------------
-- name:        validateSession
-- what:        checks if session is valid
-- author:      Frode Klevstul
-- start date:  2013.05.08
---------------------------------------------------------
function validateSession
(
	  p_session_key				in ses_session.session_key%type
	, p_ip_address				in use_ip_restriction.ip_address%type
	, p_last_code_module		in ses_session.last_code_module%type
	, p_last_parameter_list		in ses_session.last_parameter_list%type
	, p_return_message			out varchar2
) return boolean
is
	c_procedure					constant varchar2(32)					:= 'validateSession';

	v_session_pk				ses_session.pk%type;
	v_count						int;
	v_timeout					int;
	v_return					boolean									:= false;

begin
	v_session_pk := getSessionPk(p_session_key);

	-- invalid session key
	if (v_session_pk is null) then
		v_return			:= false;
		p_return_message	:= 'no pk found in ses_session for session_key ['||p_session_key||']';

	-- valid session key
	else
		-- get minutes till timeout (if 0 or negative we have a timeout)
		v_timeout := timeout(v_session_pk);

		-- if we have a timeout
		if (v_timeout <= 0) then
			v_return			:= false;
			p_return_message	:= 'session has timed out ['||v_timeout||']';

		-- no timeout
		else

			-- check if ip is valid for session (avoid someone using same session at another ip)
			select	count(*)
			into	v_count
			from	ses_session
			where	pk = v_session_pk
			and		ip_address = p_ip_address;
	
			if (v_count = 0) then
				v_return := false;
				p_return_message	:= 'session is registered with other ip address than ['||p_ip_address||']';
			else

				-- check if session has been killed
				select	count(*)
				into	v_count
				from	ses_session
				where	pk = v_session_pk
				and		bool_killed = 1;

				if (v_count = 1) then
					select	return_message
					into	p_return_message
					from	ses_session
					where	pk = v_session_pk;
					
					v_return := false;
					p_return_message	:= 'session killed ['||p_return_message||']';
				else

					update	ses_session
					set		last_code_module	= p_last_code_module
						,	last_parameter_list	= p_last_parameter_list
					where	pk					= v_session_pk;
					commit;
	
					v_return := true;
				end if;
			end if;		
		end if;
	end if;

	return v_return;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		return null;
end validateSession;


---------------------------------------------------------
-- name:        generateSessionKey
-- what:        creates and returns a session key
--				references:
--					http://www.oracle-base.com/articles/misc/dbms_random.php
--					http://psoug.org/reference/dbms_random.html
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
function generateSessionKey
	return ses_session.session_key%type
is
	c_procedure					constant varchar2(32)					:= 'generateSessionKey';

	v_session_key				ses_session.session_key%type;

begin

	-- set a new seed, using the timestamp, so it all gets more random
	dbms_random.seed (val => to_char(systimestamp, 'YYYYDDMMHH24MISSFFFF') );

	select	dbms_random.string('X',TRUNC(DBMS_RANDOM.value(20,40)))
	into	v_session_key
	from	dual;

	return v_session_key;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		return null;
end generateSessionKey;


---------------------------------------------------------
-- name:        setSession
-- what:        sets a session for given user
-- author:      Frode Klevstul
-- start date:  2013.05.08
---------------------------------------------------------
procedure setSession
(
	  p_user_pk					in use_user.pk%type
	, p_ip_address				in ses_session.ip_address%type
	, p_session_key				in ses_session.session_key%type
	, p_last_code_module		in ses_session.last_code_module%type
	, p_last_parameter_list		in ses_session.last_parameter_list%type
)
is
	c_procedure					constant varchar2(32)					:= 'setSession';

begin

	insert into ses_session
	(session_key, use_user_fk, ip_address, last_code_module, last_parameter_list, date_last_action)
	values (
		  p_session_key
		, p_user_pk
		, p_ip_address
		, p_last_code_module
		, p_last_parameter_list
		, systimestamp
	);
	commit;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		null;
end setSession;


---------------------------------------------------------
-- name:        getSessionPk
-- what:        returns a session pk given a session key
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
function getSessionPk
(
	  p_session_key				in ses_session.session_key%type

) return ses_session.pk%type
is
	c_procedure					constant varchar2(32)					:= 'getSessionPk';

	v_session_pk				ses_session.pk%type;

begin

	for r_cursor in (
		select	pk
		from	ses_session
		where	session_key = p_session_key
	) loop
		v_session_pk := r_cursor.pk;
	end loop;

	return v_session_pk;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		return null;
end getSessionPk;


---------------------------------------------------------
-- name:        timeout
-- what:        returns number of minutes till / since timeout
-- author:      Frode Klevstul
-- start date:  2013.05.06
---------------------------------------------------------
function timeout
(
	  p_session_pk				in ses_session.pk%type
) return number
is
	c_procedure					constant varchar2(32)					:= 'timeout';

	v_return					number									:= 0;
	v_date_last_action			ses_session.date_last_action%type;

	v_use_idleTime				number									:= to_number(sys_lib.getVariableValue('timeout'));
    v_rounded_value				number (20, 0)							:= null;				-- number with no decimals
	v_timeout					date;
	v_count						int;

begin

	-- find the timestamp for the session's last action
	for r_cursor in (
		select	date_last_action
		from	ses_session
		where	pk = p_session_pk
	) loop
		v_date_last_action := r_cursor.date_last_action;
	end loop;

	-- if we have no value we have given the wrong session key
	if (v_date_last_action is null) then
		raise_application_error(-20000, 'no date_last_action found for ses_session.pk = ['||p_session_pk||']');
	end if;

	-- check if session has got an individual timeout value
	select	count(*)
	into	v_count
	from	ses_session
	where	pk = p_session_pk
	and		timeout is not null;
	
	-- if session has got its own timemout, then use it...
	if (v_count > 0) then
		select	timeout
		into	v_use_idleTime
		from	ses_session
		where	pk = p_session_pk;
	end if;

	-- calculate the time when the user got / will get timeout
	v_timeout := ( v_date_last_action + (v_use_idleTime/1440) );

	-- calculate if we have a timeout or not, if this number is bigger than 0 we do not have timeout yet
    v_rounded_value := to_number(to_char(v_timeout,'YYYYMMDDHH24MISS')) - to_number(to_char(sysdate,'YYYYMMDDHH24MISS'));
    v_return		:= v_rounded_value;

	-- if we don't have timeout we update the time of last action
    if ( v_return > 0 ) then
		update	ses_session
		set		date_last_action	= sysdate
		where 	pk					= p_session_pk;
		commit;

	end if;

	-- return the timeout, which is a positive or negative number
	return v_return;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		return null;
end timeout;





-- ******************************************** --
end;
/
show errors;