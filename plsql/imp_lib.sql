PROMPT *** imp_lib ***
set define off
set serveroutput on





-- ******************************************************
-- *													*
-- *				package head						*
-- *													*
-- ******************************************************
create or replace package imp_lib is
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- WARNING: Never edit this code directly using TOAD or another Oracle tool.
	--          The updates should always be done editing the original script, and
	--          then be compiled into the database using TOAD or SQL*Plus !!!
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	-- Package overview:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- name: imp_lib
	--
	-- Package related the import module.
	--
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- How to make comments in the code:
	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

	-- ex 1:
	-- ----------------------------------------------------------------------
	-- Comment a bulk of code
	-- For example if you want to comment the next portion of the code, like
	-- an while loop, if statment or something similar.
	-- ----------------------------------------------------------------------

	-- ex 2:
	-- ---
	-- important comment for the next line(s) of code
	-- ---

	-- ex 3:
	-- comment the next line(s) of code

	-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	--
	-- *** Revision | Newest version at the top
	--
	-- 130210 : Frode Klevstul : started
	--
	-- +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	procedure i;
	procedure run;
	procedure import_sleepbot_dump;
	procedure import_sleepbot;

end;
/
show errors;





-- ******************************************************
-- *													*
-- *				package body						*
-- *													*
-- ******************************************************
create or replace package body imp_lib is

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- global variables
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
	g_package	constant varchar2(16)	:= 'imp_lib';
	g_debug		constant boolean		:= false;

-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- procedures and functions
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++

---------------------------------------------------------
-- name:        i(nfo)
-- what:        info procedure
-- author:      Frode Klevstul
-- start date:  2013.02.09
---------------------------------------------------------
procedure i
is
	c_procedure					constant varchar2(32)					:= 'i';
begin
	dbms_output.put_line(to_char(sysdate,'YYMMDD HH24:MI:SS') ||': '||g_package||'.'||c_procedure);
end i;


---------------------------------------------------------
-- name:        run
-- what:        run everything
-- author:      Frode Klevstul
-- start date:  2013.02.14
---------------------------------------------------------
procedure run
is
	c_procedure					constant varchar2(32)					:= 'run';
begin
	import_sleepbot_dump;
	import_sleepbot;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		null;
end run;


---------------------------------------------------------
-- name:        import_sleepbot_dump
-- what:        import data from imp_sleepbot to rec_sleep_record
-- author:      Frode Klevstul
-- start date:  2013.02.10
---------------------------------------------------------
procedure import_sleepbot_dump
is
	c_procedure					constant varchar2(32)					:= 'import_sleepbot_dump';
	c_date_format_old			constant varchar2(8)					:= 'YY/MM/DD';
	c_date_format_new			constant varchar2(8)					:= 'MM/DD/YY';
	-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
	c_date_format				constant varchar2(8)					:= c_date_format_new;
	-- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

	v_timestamp_sleep			imp_sleepbot.timestamp_sleep%type;
	v_timestamp_wake			imp_sleepbot.timestamp_wake%type;
	v_fix_message				imp_sleepbot.fix_message%type;
	v_hours						imp_sleepbot_dump.hours%type;
	v_timestamp_wake_hours_min	timestamp;
	v_timestamp_wake_hours_max	timestamp;
	v_bool_daylight_saving		imp_sleepbot.bool_daylight_saving%type;
	v_bool_standard_time		imp_sleepbot.bool_standard_time%type;
	v_rownum					int										:= 0;
	v_insertednum				int										:= 0;
	v_count						int;

begin
	-- -----------------------------------------------
	-- loop through all data stored in dump table
	-- -----------------------------------------------
	for r_cursor in (
		select	sleep_date
			,	sleep_time
			,	wake_time
			,	hours
			,	note
			,	rownum
		from	imp_sleepbot_dump
	) loop
		v_rownum					:= r_cursor.rownum;
		v_hours						:= r_cursor.hours;					-- we might do a replace, so has to store this in a variable
		v_fix_message				:= '';								-- reset
		v_bool_daylight_saving		:= null;							-- reset
		v_bool_standard_time		:= null;							-- reset
		v_timestamp_wake_hours_min	:= null;
		v_timestamp_wake_hours_max	:= null;

--		dbms_output.put_line(r_cursor.sleep_date||' | '||r_cursor.sleep_time||' | '||r_cursor.wake_time||' | '||r_cursor.hours||'------------');

		-- ---
		-- convert varchar2 log data into oracle timestamps
		-- ---
		select	to_timestamp(r_cursor.sleep_date||' '||r_cursor.sleep_time, c_date_format||' HH24:MI')
		into	v_timestamp_sleep
		from	dual;

		select	to_timestamp(r_cursor.sleep_date||' '||r_cursor.wake_time, c_date_format||' HH24:MI')
		into	v_timestamp_wake
		from	dual;

		-- ---
		-- a separate date for wake time is not stored by sleeptracker,
		-- so in many cases we wake up the date after we go to sleep.
		-- normally this is the case if we go to bed before midnight
		-- this won't be correct if we sleep for more than 24 hours in one go,
		-- but (most likely) that will never happen
		-- ---
		if (v_timestamp_wake < v_timestamp_sleep) then
			v_timestamp_wake	:= v_timestamp_wake + interval '1' day;
			v_fix_message		:= v_fix_message || ' : one day added to timestamp_wake';
		end if;

		-- ---
		-- several hours records do have an E in them, like '1.5E', '2.0E', '7.5E'
		-- for these records we simply substitute the 'E' with a zero.
		-- (this must be due to a bug in sleeptracker)
		-- ---
		if ( instr(v_hours, 'E') > 0 ) then
			v_hours			:= replace(v_hours, 'E', '0');
			v_fix_message	:= v_fix_message || ' : E in hours <'||r_cursor.hours||'> replaced with zero';
		end if;

		-- --------------------------------------------------------------------
		-- check to see if sleep time + hours slept matches wake time
		-- this might not be the case, partly due to sleepbot bugs, and also
		-- due to switches to / from daylight saving during sleeps
		-- --------------------------------------------------------------------

		-- calculating wake time by using sleep time plus hours, adding and subtracting 2 minutes (+/- 2 minutes) to give the recorded value some slack
		v_timestamp_wake_hours_min := v_timestamp_sleep + numtodsinterval( v_hours, 'hour') - numtodsinterval( 2, 'minute' );
		v_timestamp_wake_hours_max := v_timestamp_sleep + numtodsinterval( v_hours, 'hour') + numtodsinterval( 2, 'minute' );

		-- make sure the wake timestamp equals the sleep timestamp plus hours slept (+/- 2 minutes)
		if not (
				v_timestamp_wake > v_timestamp_wake_hours_min
			and	v_timestamp_wake < v_timestamp_wake_hours_max
		) then

			-- ---
			-- for several zero-sleep entries hours wrongly gets a value different to zero
			-- we're fixing this by simply setting hours to zero
			-- ---
			if (v_timestamp_sleep = v_timestamp_wake) then
				v_hours			:= 0;
				v_fix_message	:= v_fix_message || ' : hours adjusted from <'||r_cursor.hours||'> to zero';
			end if;

			-- ---
			-- if it's not a zero-sleep entry...
			-- ---
			if (v_timestamp_sleep != v_timestamp_wake) then
				-- ---
				-- check if we have a switch to daylight saving (turning clock one hour forward)
				-- if yes we have March as month (month number 3),
				-- and that hours slept is one hour LESS than wake time - sleep time
				-- eg: sleep from 00:00 till 08:00 equals 7 hours of sleep
				-- ---
				if (
						extract(month from v_timestamp_sleep) = 3
					and v_timestamp_wake > v_timestamp_wake_hours_min + numtodsinterval( 1, 'hour')
					and v_timestamp_wake < v_timestamp_wake_hours_max + numtodsinterval( 1, 'hour')
				) then
					v_bool_daylight_saving	:= 1;
					v_fix_message			:= v_fix_message || ' : sleep during switch to daylight saving';
				end if;
		
				-- ---
				-- check if we have a switch to standard time (turning clock one hour back)
				-- if yes we have October as month (month number 10),
				-- and that hours slept is one hour MORE than wake time - sleep time
				-- eg: sleep from 00:00 till 08:00 equals 9 hours of sleep
				-- ---
				if (
						extract(month from v_timestamp_sleep) = 10
					and v_timestamp_wake > v_timestamp_sleep + numtodsinterval(r_cursor.hours, 'hour') - numtodsinterval( 1, 'hour') - numtodsinterval( 2, 'minute' )
					and v_timestamp_wake < v_timestamp_sleep + numtodsinterval(r_cursor.hours, 'hour') - numtodsinterval( 1, 'hour') + numtodsinterval( 2, 'minute' )
				) then
					v_bool_standard_time	:= 1;
					v_fix_message			:= v_fix_message || ' : sleep during switch to standard time';
				end if;
			end if;
		end if;

		-- ----------------------------------------------
		-- move data to imp_sleepbot table
		-- ----------------------------------------------

		-- ---
		-- check if the same entry has been added earlier,
		-- avoid duplicates
		-- ---
		select	count(*)
		into	v_count
		from	imp_sleepbot
		where	timestamp_sleep = v_timestamp_sleep
		and		timestamp_wake = v_timestamp_wake;

		if (v_count = 0) then
			v_insertednum := v_insertednum + 1;
			insert into imp_sleepbot
			(
				 	timestamp_sleep
				,	timestamp_wake
				,	hours
				,	note
				, 	fix_message
				,	bool_daylight_saving
				,	bool_standard_time
			)
			values(
					v_timestamp_sleep
				,	v_timestamp_wake
				,	v_hours
				,	r_cursor.note
				, 	v_fix_message
				,	v_bool_daylight_saving
				,	v_bool_standard_time
			);
			commit;
		end if;
	end loop;

	dbms_output.put_line(v_rownum||' rows processed');
	dbms_output.put_line(v_insertednum||' rows inserted');

	delete from imp_sleepbot_dump;
	commit;

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		null;
end import_sleepbot_dump;


---------------------------------------------------------
-- name:        import_sleepbot
-- what:        import data from imp_sleepbot to rec_sleep_record
-- author:      Frode Klevstul
-- start date:  2013.02.13
---------------------------------------------------------
procedure import_sleepbot
is
	c_procedure					constant varchar2(32)					:= 'import_sleepbot';

	v_rownum					int										:= 0;
	v_minutes					rec_sleep_record.minutes%type;
	v_pk						rec_sleep_record.pk%type;

begin

	for r_cursor in (
		select	pk
			,	timestamp_sleep
			,	timestamp_wake
			,	hours
			,	note
			,	bool_daylight_saving
			,	bool_standard_time
		from	imp_sleepbot
		where	bool_imported is null
		and		bool_ignore is null
		order by timestamp_sleep asc
	) loop
		-- note: since we're using "order by" in the SQL we can't use rownum from the select statement for counting (as the order that will be presented might very well not be ascending)
		v_rownum := v_rownum +1;

		-- in our main table we store minutes in stead of hours
		v_minutes := round(r_cursor.hours * 60, 0);

		-- check if we have any entries with same sleep or wake time
		select	max(pk)
		into	v_pk
		from	rec_sleep_record
		where	timestamp_sleep = r_cursor.timestamp_sleep
		or		timestamp_wake = r_cursor.timestamp_wake;

		-- if there exists a conflicting row in rec_sleep_record
		if (v_pk is not null) then
			update	imp_sleepbot
			set		ignore_message	= 'row is in conflict with row rec_sleep_record.pk <'||v_pk||'> '
				,	bool_ignore		= 1
			where	pk = r_cursor.pk;

		-- copy value over to rec_sleep_record
		else
			insert into rec_sleep_record
			(
					timestamp_sleep
				,	timestamp_wake
				,	minutes
				,	note
				,	bool_daylight_saving
				,	bool_standard_time
			)
			values
			(
					r_cursor.timestamp_sleep
				,	r_cursor.timestamp_wake
				,	v_minutes
				,	r_cursor.note
				,	r_cursor.bool_daylight_saving
				,	r_cursor.bool_standard_time
			);
	
			-- mark row as imported
			update	imp_sleepbot
			set		bool_imported = 1
			where	pk = r_cursor.pk;
		end if;
		
		commit;
	end loop;

	dbms_output.put_line(v_rownum||' rows processed');

exception
	when others then
		log_lib.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
		null;
end import_sleepbot;





-- ******************************************** --
end;
/
show errors;