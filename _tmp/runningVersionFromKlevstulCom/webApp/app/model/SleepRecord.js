/*
	what:		Model for a sleep record
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)

*/

Ext.define('SleepTracker.model.SleepRecord', {
    extend: 'Ext.data.Model',

	fields: [
        {name: 'pk',  					type: 'int'},
        {name: 'timestamp_sleep', 		type: 'date', 		dateFormat: 'd.m.Y H:i'},
        {name: 'timestamp_wake', 		type: 'date', 		dateFormat: 'd.m.Y H:i'},
        {name: 'minutes', 				type: 'int'},
        {name: 'note', 					type: 'string'},
		{name: 'timezone_adjustment',	type: 'int'},
		{name: 'bool_daylight_saving', 	type: 'int'},
		{name: 'bool_standard_time', 	type: 'int'},
        {name: 'timestamp_inserted', 	type: 'date', 		dateFormat: 'd.m.Y'},
        {name: 'timestamp_updated', 	type: 'date', 		dateFormat: 'd.m.Y'}
    ],
    proxy: {
        type: 'memory',//'ajax',
        //url: 'data/commissions.json',
        reader: {
            type: 'json',
            root: 'results'
        }
    }
});


