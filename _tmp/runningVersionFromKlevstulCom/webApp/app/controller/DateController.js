/*
	what:		Dates to control displayed data
	started:	March 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.DateController', {
    extend: 'Ext.Container',
    alias: 'widget.datecontroller',
	layout: {
		type: 'column'
	},
	margin: '10 10 10 10',
    items: [{
        xtype: 'datefield',
        fieldLabel: 'Start date',
		format: 'd.m.Y',
		itemId: 'datefield-startdate',
		labelWidth: 65,
		width: 180
    },{
        xtype: 'datefield',
		margin: '0 0 0 10',
        fieldLabel: 'End date',
		itemId: 'datefield-enddate',
		format: 'd.m.Y',
		labelWidth: 65,
		width: 180    
    },{
		margin: '0 0 0 10',
		html: '[past 10d] [past 30d] [past 60d] [past 180d] [past 365d] [all]'
	},{
		xtype: 'button',
		margin: '0 0 0 10',
		text: 'Submit',
		listeners: {
			click: function() {
				var fromDate	= this.ownerCt.getComponent('datefield-startdate').getValue();
				var toDate		= this.ownerCt.getComponent('datefield-enddate').getValue();
				var stores		= this.ownerCt.getStores();

				Ext.each(stores, function(store){

					if (fromDate !== undefined && toDate !== undefined && fromDate !== null && toDate !== null){
						console.log('filtering store');
						store.filterBy(
							function( rec, id ){
								return rec.data.timestamp_sleep >= fromDate && rec.data.timestamp_sleep < toDate;
							}
						);
					} else {
						console.log('set filter to always returning true');
						store.filterBy(function( rec, id ){return true;});
					}

				});
			}
		}
	}
	]
	,
	getStores: function(){
		var stores = [];
		stores.push(Ext.ComponentQuery.query('sleeprecordgrid')[0].getStore());
		stores.push(Ext.ComponentQuery.query('sleeprecorddaygrid')[0].getStore());
		
		return stores;
		//return this.up('mainlayout').getComponent('mainlayout_west').getComponent('sleeprecordgrid').getStore();
	}

});