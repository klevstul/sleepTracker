/*
	what:		Present all sleep records in a grid
	started:	April 2013
	who:		Frode Klevstul (frode at klevstul dot com)
*/

Ext.define('SleepTracker.view.SleepRecordDayGrid', {
    extend: 'Ext.grid.Panel',
	itemId: 'sleeprecorddaygrid',
	alias: 'widget.sleeprecorddaygrid',
	//stateId: 'commissionListGrid',
	viewConfig: {
		stripeRows: true
	},
	store: 'SleepRecordDays',
	border: false,
	layout: 'fit',

plugins: [
        Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2
        })
    ],
	columns: [
		{
			text     : 'PK',
			width    : 40,
			sortable : true,
			dataIndex: 'pk'
		},
		{
			text     : 'date',
			width    : 115,
			sortable : true,
			dataIndex: 'timestamp_sleep',
			renderer	: Ext.util.Format.dateRenderer('d.m.Y H:i')
		},
		{
			text     : 'Min',
			width    : 40,
			sortable : true,
			dataIndex: 'minutes'
		}
	],

	listeners: {

		selectionchange: function(model, records) {
			console.log('selectionchange ...');

			var chart = Ext.ComponentQuery.query('trendview')[0];
			var series = chart.series.items[0];
			var selectedIndex = records[0].index;
			var object = series.items[ selectedIndex ];

			series.eachRecord( function(){ series.unHighlightItem(this); }, this);
			
			series.highlightItem( object );
		}
		,

		itemdblclick: function(this_, record, item, index, e, eOpts) {
			console.log('double click');
			console.log( record.data.pk );
			
			var view = Ext.ComponentQuery.query('gridmainview')[0];
			var grid = Ext.ComponentQuery.query('sleeprecordgrid')[0];
			var store = grid.getStore();
			var sm = grid.getSelectionModel();

			
			var rows = store.getRange();
			//console.log( rows );
			view.setActiveTab(1);
								
			//sm.select( 2 , true);

			Ext.each(rows, function(row) {
				if(row.data.pk === record.data.pk){
				  var idx = row.index;
				  sm.select(idx, true);
				}
			});
			
			//console.log(grid);
		}

	}
	,


	initComponent: function() {
		console.log('SleepRecordDayGrid()'); 
		this.callParent();
	}

});
